﻿Public Class frmMain
    Dim globalstrIP As String
    Dim publicStrIP As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strHostName As String
        Dim strIPAddress As String
        Dim setIPs As Array

        strHostName = Net.Dns.GetHostName()
        'strIPAddress = System.Net.Dns.GetHostByName(strHostName).AddressList(1).ToString()
        'MessageBox.Show("Host Name: " & strHostName & "; IP Address: " & strIPAddress)
        setIPs = Net.Dns.GetHostByName(strHostName).AddressList()

        For Each adapterip In setIPs
            Console.WriteLine("{0}", adapterip)
            If adapterip.ToString.IndexOf("192.168.1") <> -1 Then
                strIPAddress &= adapterip.ToString & vbNewLine
                globalstrIP = adapterip.ToString
            End If
        Next adapterip

        lblLocalIP.Text = "Your Local IP: " & globalstrIP



        lblCurrentIP.Text = strIPAddress
        txtHiddenIP.Text = strIPAddress

        Dim IP As New Net.WebClient
        On Error Resume Next   ' Defer error trapping.
        publicStrIP = IP.DownloadString("http://geekbacolod.com/myip.php").ToString

        Select Case publicStrIP
            Case "119.92.231.131"
                lblCurrentIP.Text = "MAIN CONNECTION" & vbNewLine & "[119.92.231.131]"
            Case "122.55.223.242"
                lblCurrentIP.Text = "BACK-UP CONNECTION (PLDT)" & vbNewLine & "[122.55.223.242]"
            Case "124.6.139.98"
                lblCurrentIP.Text = "BACK-UP CONNECTION (GLOBE)" & vbNewLine & "[124.6.139.98]"
            Case Else
                lblCurrentIP.Text = "NOT LISTED or NO INTERNET CONNECTION!" & vbNewLine & "(or maybe you're on a VPN)"

        End Select
        ' 122.54.132.132 = MAIN CONNECTION
        ' 122.55.223.242 = PLDT Leased Line
        'Mark Ramos

        Dim adapters_net As Net.NetworkInformation.NetworkInterface() = Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()
        Dim adapter_net As Net.NetworkInformation.NetworkInterface
        For Each adapter_net In adapters_net
            Dim properties_net As Net.NetworkInformation.IPInterfaceProperties = adapter_net.GetIPProperties()
            Console.WriteLine(adapter_net.Name)
            Console.WriteLine(adapter_net.Description)
            Console.WriteLine("  DNS suffix............................... : {0}", properties_net.DnsSuffix)
            Console.WriteLine("  DNS enabled ............................. : {0}", properties_net.IsDnsEnabled)
            Console.WriteLine("  Dynamically configured DNS .............. : {0}", properties_net.IsDynamicDnsEnabled)
            Console.WriteLine("========================================================================================")
        Next adapter_net

    End Sub


    Private Sub btnMainPLDT_Click(sender As Object, e As EventArgs) Handles btnMainPLDT.MouseClick

        Dim btn_result As Integer

        btn_result = MessageBox.Show("Are you sure you want to Switch to MAIN CONNECTION?", "Warning!", MessageBoxButtons.YesNo)
        If btn_result = DialogResult.Yes Then
            btnMainPLDT.Text = "CONFIGURING..."
            Shell("CMD.exe")
            'SendKeys.Send("netsh interface ip set address ""Ethernet"" static " & strIPAddress & "255.255.255.0 192.168.1.253 1 ")
            System.Threading.Thread.Sleep(2000)
            SendKeys.Send("C:")
            SendKeys.Send("{ENTER}")
            SendKeys.Send("cd /")
            SendKeys.Send("{ENTER}")
            SendKeys.Send("netsh Int ip set address name = ""Ethernet"" source = dhcp")
            SendKeys.Send("{ENTER}")
            SendKeys.Send("ipconfig /renew")
            SendKeys.Send("{ENTER}")
            'SendKeys.Send("MSG * Done Setting your Network! You're now connected to our MAIN Gateway ")
            SendKeys.Send("{ENTER}")
            SendKeys.Send("exit")
            SendKeys.Send("{ENTER}")
            btnMainPLDT.Text = "MAIN CONNECTION ( PLDT )"
            MessageBox.Show("Done Setting your Network!" & vbNewLine & "You're now connected to our MAIN CONNECTION" & vbNewLine & "(If internet is still not connected. Please wait for a couple of minutes!)")
            lblCurrentIP.Text = "MAIN CONNECTION" & vbNewLine & "[119.92.231.131]"
        End If
    End Sub

    Private Sub btnPLDTLeasedLine_MouseClick(sender As Object, e As MouseEventArgs) Handles btnPLDTLeasedLine.MouseClick
        Dim netCommand As String

        Dim btn_result As Integer

        btn_result = MessageBox.Show("Are you sure you want to Switch to BACK-UP CONNECTION (PLDT)?", "Warning!", MessageBoxButtons.YesNo)
        If btn_result = DialogResult.Yes Then
            btnPLDTLeasedLine.Text = "CONFIGURING..."
            Shell("CMD.exe")
            'SendKeys.Send("netsh interface ip set address ""Ethernet"" static " & strIPAddress & "255.255.255.0 192.168.1.253 1 ")
            System.Threading.Thread.Sleep(2000)
            SendKeys.Send("C:")
            SendKeys.Send("{ENTER}")
            SendKeys.Send("cd /")
            SendKeys.Send("{ENTER}")

            netCommand = "netsh interface ip set address ""Ethernet"" static " & globalstrIP & " 255.255.255.0 192.168.1.253 1 "
            SendKeys.Send(netCommand)
            SendKeys.Send("{ENTER}")

            SendKeys.Send("netsh Int ipv4 set dns name=""Ethernet"" static 8.8.8.8 primary validate=no")
            SendKeys.Send("{ENTER}")

            SendKeys.Send("netsh Int ipv4 add dns name=""Ethernet"" 8.8.4.4 index=2 validate=no")
            SendKeys.Send("{ENTER}")

            'SendKeys.Send("MSG * Done Setting your Network! You're now connected to our PLDT Leased Line Gateway")
            SendKeys.Send("{ENTER}")
            SendKeys.Send("exit")
            SendKeys.Send("{ENTER}")
            btnPLDTLeasedLine.Text = "BACK-UP CONNECTION ( PLDT )"
            MessageBox.Show("Done Setting your Network!" & vbNewLine & "You're now connected to our BACK-UP CONNECTION (PLDT)")
            lblCurrentIP.Text = "BACK-UP CONNECTION (PLDT)" & vbNewLine & "[122.55.223.242]"
        End If
    End Sub

    Private Sub btnGlobe20MB_MouseClick(sender As Object, e As MouseEventArgs) Handles btnGlobe20MB.MouseClick
        Dim netCommand As String

        Dim btn_result As Integer

        btn_result = MessageBox.Show("Are you sure you want to Switch to BACK-UP CONNECTION (GLOBE)?", "Warning!", MessageBoxButtons.YesNo)
        If btn_result = DialogResult.Yes Then
            btnGlobe20MB.Text = "CONFIGURING..."
            Shell("CMD.exe")
            'SendKeys.Send("netsh interface ip set address ""Ethernet"" static " & strIPAddress & "255.255.255.0 192.168.1.253 1 ")
            System.Threading.Thread.Sleep(2000)
            SendKeys.Send("C:")
            SendKeys.Send("{ENTER}")
            SendKeys.Send("cd /")
            SendKeys.Send("{ENTER}")

            netCommand = "netsh interface ip set address ""Ethernet"" static " & globalstrIP & " 255.255.255.0 192.168.1.254 1 "
            SendKeys.Send(netCommand)
            SendKeys.Send("{ENTER}")

            SendKeys.Send("netsh Int ipv4 set dns name=""Ethernet"" static 8.8.8.8 primary validate=no")
            SendKeys.Send("{ENTER}")

            SendKeys.Send("netsh Int ipv4 add dns name=""Ethernet"" 8.8.4.4 index=2 validate=no")
            SendKeys.Send("{ENTER}")

            'SendKeys.Send("MSG * Done Setting your Network! You're now connected to our PLDT Leased Line Gateway")
            SendKeys.Send("{ENTER}")
            SendKeys.Send("exit")
            SendKeys.Send("{ENTER}")
            btnGlobe20MB.Text = "BACK-UP CONNECTION ( GLOBE )"
            MessageBox.Show("Done Setting your Network!" & vbNewLine & "You're now connected to our BACK-UP CONNECTION (GLOBE)")
            lblCurrentIP.Text = "BACK-UP CONNECTION (GLOBE)" & vbNewLine & "[124.6.139.98]"
        End If
    End Sub
End Class
