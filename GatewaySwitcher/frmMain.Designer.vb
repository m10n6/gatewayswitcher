﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblCurrentIP = New System.Windows.Forms.Label()
        Me.txtHiddenIP = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnGlobe20MB = New System.Windows.Forms.Button()
        Me.btnPLDTLeasedLine = New System.Windows.Forms.Button()
        Me.btnMainPLDT = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblLocalIP = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(2, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(173, 22)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "You're Connected to :"
        '
        'lblCurrentIP
        '
        Me.lblCurrentIP.AutoSize = True
        Me.lblCurrentIP.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentIP.Location = New System.Drawing.Point(181, 12)
        Me.lblCurrentIP.Name = "lblCurrentIP"
        Me.lblCurrentIP.Size = New System.Drawing.Size(45, 18)
        Me.lblCurrentIP.TabIndex = 1
        Me.lblCurrentIP.Text = "0.0.0.0"
        '
        'txtHiddenIP
        '
        Me.txtHiddenIP.Location = New System.Drawing.Point(331, 12)
        Me.txtHiddenIP.Name = "txtHiddenIP"
        Me.txtHiddenIP.Size = New System.Drawing.Size(110, 20)
        Me.txtHiddenIP.TabIndex = 3
        Me.txtHiddenIP.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Open Sans", 7.0!)
        Me.Label2.Location = New System.Drawing.Point(267, 317)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(174, 14)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Powered By: Pathcutters Phils. Inc."
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnGlobe20MB)
        Me.GroupBox1.Controls.Add(Me.btnPLDTLeasedLine)
        Me.GroupBox1.Controls.Add(Me.btnMainPLDT)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(37, 67)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(390, 247)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        '
        'btnGlobe20MB
        '
        Me.btnGlobe20MB.Font = New System.Drawing.Font("Open Sans", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGlobe20MB.Location = New System.Drawing.Point(71, 152)
        Me.btnGlobe20MB.Name = "btnGlobe20MB"
        Me.btnGlobe20MB.Size = New System.Drawing.Size(247, 40)
        Me.btnGlobe20MB.TabIndex = 8
        Me.btnGlobe20MB.Text = "BACK-UP CONNECTION ( GLOBE )"
        Me.btnGlobe20MB.UseVisualStyleBackColor = True
        '
        'btnPLDTLeasedLine
        '
        Me.btnPLDTLeasedLine.Font = New System.Drawing.Font("Open Sans", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPLDTLeasedLine.Location = New System.Drawing.Point(71, 100)
        Me.btnPLDTLeasedLine.Name = "btnPLDTLeasedLine"
        Me.btnPLDTLeasedLine.Size = New System.Drawing.Size(247, 40)
        Me.btnPLDTLeasedLine.TabIndex = 5
        Me.btnPLDTLeasedLine.Text = "BACK-UP CONNECTION ( PLDT )"
        Me.btnPLDTLeasedLine.UseVisualStyleBackColor = True
        Me.btnPLDTLeasedLine.Visible = False
        '
        'btnMainPLDT
        '
        Me.btnMainPLDT.Font = New System.Drawing.Font("Open Sans", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMainPLDT.Location = New System.Drawing.Point(71, 46)
        Me.btnMainPLDT.Name = "btnMainPLDT"
        Me.btnMainPLDT.Size = New System.Drawing.Size(247, 40)
        Me.btnMainPLDT.TabIndex = 6
        Me.btnMainPLDT.Text = "MAIN CONNECTION ( PLDT )"
        Me.btnMainPLDT.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Open Sans", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(134, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 15)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "SELECT CONNECTION"
        '
        'lblLocalIP
        '
        Me.lblLocalIP.AutoSize = True
        Me.lblLocalIP.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocalIP.Location = New System.Drawing.Point(3, 51)
        Me.lblLocalIP.Name = "lblLocalIP"
        Me.lblLocalIP.Size = New System.Drawing.Size(88, 18)
        Me.lblLocalIP.TabIndex = 8
        Me.lblLocalIP.Text = "Your Local IP:"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(453, 342)
        Me.Controls.Add(Me.lblLocalIP)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtHiddenIP)
        Me.Controls.Add(Me.lblCurrentIP)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gateway Switcher"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents lblCurrentIP As Label
    Friend WithEvents txtHiddenIP As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents btnMainPLDT As Button
    Friend WithEvents btnPLDTLeasedLine As Button
    Friend WithEvents btnGlobe20MB As Button
    Friend WithEvents lblLocalIP As Label
End Class
